-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: ix.cs.uoregon.edu    Database: zookeeper
-- ------------------------------------------------------
-- Server version	5.6.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `anim`
--

DROP TABLE IF EXISTS `anim`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anim` (
  `anim_id` int(11) NOT NULL AUTO_INCREMENT,
  `anim_nm` varchar(100) NOT NULL,
  `spec_id` int(11) NOT NULL,
  `feed_sched_id` int(11) NOT NULL,
  `aquire_dtm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `dob` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `sex` char(1) NOT NULL,
  PRIMARY KEY (`anim_id`),
  KEY `fk_animal_animal_type_idx` (`spec_id`),
  KEY `fk_anim_feed_sched1_idx` (`feed_sched_id`),
  CONSTRAINT `fk_animal_animal_type` FOREIGN KEY (`spec_id`) REFERENCES `anim_spec` (`anim_spec_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_anim_feed_sched1` FOREIGN KEY (`feed_sched_id`) REFERENCES `feed_sched` (`sched_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anim`
--

LOCK TABLES `anim` WRITE;
/*!40000 ALTER TABLE `anim` DISABLE KEYS */;
INSERT INTO `anim` VALUES (1,'Tigger the Tiger',1,1,'2015-11-19 17:41:54','1992-11-26 00:00:00','m'),(2,'Tango',1,2,'2015-11-19 17:45:26','2015-11-19 17:45:26','m'),(3,'Django',1,1,'2015-11-17 23:54:36','2015-11-19 23:54:36','m'),(4,'Tina',1,1,'2015-11-24 00:00:00','1990-05-01 00:00:00','f'),(5,'Shang',5,1,'2015-11-25 00:00:00','1990-05-01 00:00:00','m'),(6,'Yao',5,1,'2015-11-25 00:00:00','1996-01-23 00:00:00','f'),(7,'Kipenzi',6,1,'0000-00-00 00:00:00','2014-04-24 00:00:00','f');
/*!40000 ALTER TABLE `anim` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 19:27:05
