-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: ix.cs.uoregon.edu    Database: zookeeper
-- ------------------------------------------------------
-- Server version	5.6.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `keeper_spec`
--

DROP TABLE IF EXISTS `keeper_spec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `keeper_spec` (
  `keeper_emp_id` int(11) NOT NULL,
  `anim_spec_anim_spec_id` int(11) NOT NULL,
  PRIMARY KEY (`keeper_emp_id`,`anim_spec_anim_spec_id`),
  KEY `fk_keeper_has_anim_spec_anim_spec1_idx` (`anim_spec_anim_spec_id`),
  KEY `fk_keeper_has_anim_spec_keeper1_idx` (`keeper_emp_id`),
  CONSTRAINT `fk_keeper_has_anim_spec_keeper1` FOREIGN KEY (`keeper_emp_id`) REFERENCES `keeper` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_keeper_has_anim_spec_anim_spec1` FOREIGN KEY (`anim_spec_anim_spec_id`) REFERENCES `anim_spec` (`anim_spec_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `keeper_spec`
--

LOCK TABLES `keeper_spec` WRITE;
/*!40000 ALTER TABLE `keeper_spec` DISABLE KEYS */;
INSERT INTO `keeper_spec` VALUES (4,1),(7,1),(13,1),(19,1),(4,3),(19,3);
/*!40000 ALTER TABLE `keeper_spec` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 19:27:13
