-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: ix.cs.uoregon.edu    Database: zookeeper
-- ------------------------------------------------------
-- Server version	5.6.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `feed_rec`
--

DROP TABLE IF EXISTS `feed_rec`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `feed_rec` (
  `feed_dtm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `emp_id` int(11) NOT NULL,
  `anim_id` int(11) NOT NULL,
  `feed_amt_lbs` float NOT NULL,
  `food_ty_id` int(11) NOT NULL,
  `last_mod_dtm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`feed_dtm`,`emp_id`,`anim_id`),
  KEY `fk_animal_has_employee_animal1_idx` (`anim_id`),
  KEY `fk_feed_record_keeper1_idx` (`emp_id`),
  KEY `food_ty_idx` (`food_ty_id`),
  CONSTRAINT `fk_animal_has_employee_animal1` FOREIGN KEY (`anim_id`) REFERENCES `anim` (`anim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_feed_record_keeper1` FOREIGN KEY (`emp_id`) REFERENCES `keeper` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `feed_rec`
--

LOCK TABLES `feed_rec` WRITE;
/*!40000 ALTER TABLE `feed_rec` DISABLE KEYS */;
INSERT INTO `feed_rec` VALUES ('2015-11-19 00:00:00',4,1,8.5,1,'2015-11-29 00:05:50'),('2015-11-19 00:00:00',4,3,7,1,'2015-11-22 18:57:46'),('2015-11-19 13:00:00',4,1,7,1,'2015-11-29 01:13:40'),('2015-11-19 14:30:00',6,2,39,1,'2015-11-22 17:28:48'),('2015-11-22 00:00:00',4,3,7,1,'2015-11-22 18:58:32'),('2015-11-22 13:00:00',4,2,7,1,'2015-11-22 18:59:07'),('2015-11-22 16:00:00',4,3,7,1,'2015-11-22 18:59:31'),('2015-11-29 01:34:44',4,3,6.5,1,'2015-11-29 01:34:44'),('2015-11-29 01:43:00',18,1,8,1,'2015-11-29 01:43:00'),('2015-12-01 00:49:10',4,6,55,9,'2015-12-01 00:49:10'),('2015-12-01 10:00:25',7,3,25,1,'2015-12-01 10:00:25'),('2015-12-01 10:00:37',4,3,45,1,'2015-12-01 10:00:37'),('2015-12-01 10:01:06',4,5,54,9,'2015-12-01 10:01:06'),('2015-12-01 10:05:58',4,5,78,8,'2015-12-01 10:05:58'),('2015-12-01 10:40:13',1,7,10,10,'2015-12-01 10:40:13'),('2015-12-01 10:40:19',1,1,20,1,'2015-12-01 10:40:19'),('2015-12-01 10:40:23',1,4,20,1,'2015-12-01 10:40:23'),('2015-12-01 19:22:23',7,3,8,1,'2015-12-01 19:22:23');
/*!40000 ALTER TABLE `feed_rec` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`henkhaus`@`%`*/ /*!50003 TRIGGER `zookeeper`.`feed_rec_AFTER_UPDATE` BEFORE UPDATE ON `feed_rec` FOR EACH ROW
BEGIN
	SET new.last_mod_dtm := NOW();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 19:27:08
