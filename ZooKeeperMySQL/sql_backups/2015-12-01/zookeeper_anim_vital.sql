-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: ix.cs.uoregon.edu    Database: zookeeper
-- ------------------------------------------------------
-- Server version	5.6.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `anim_vital`
--

DROP TABLE IF EXISTS `anim_vital`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `anim_vital` (
  `create_dtm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `anim_anim_id` int(11) NOT NULL,
  `emp_id` int(11) NOT NULL,
  `hr` int(3) DEFAULT NULL,
  `respr` int(3) DEFAULT NULL,
  `temp` float(5,2) DEFAULT NULL,
  `wt_lbs` float(10,2) DEFAULT NULL,
  `notes` varchar(500) DEFAULT NULL,
  `last_mod_dtm` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`create_dtm`,`anim_anim_id`),
  KEY `fk_anim_vital_anim1_idx` (`anim_anim_id`),
  KEY `fk_anim_vital_vet1_idx` (`emp_id`),
  CONSTRAINT `fk_anim_vital_anim1` FOREIGN KEY (`anim_anim_id`) REFERENCES `anim` (`anim_id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_anim_vital_vet1` FOREIGN KEY (`emp_id`) REFERENCES `vet` (`emp_id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `anim_vital`
--

LOCK TABLES `anim_vital` WRITE;
/*!40000 ALTER TABLE `anim_vital` DISABLE KEYS */;
INSERT INTO `anim_vital` VALUES ('2015-11-01 00:00:00',6,5,115,35,99.00,2005.00,'Looking good','2015-12-01 19:01:50'),('2015-12-01 00:00:00',1,5,98,23,98.20,849.30,'this is a test','2015-11-30 22:45:16'),('2015-12-01 00:00:00',5,5,120,32,102.00,999.99,'GREAT!','2015-12-01 18:57:12');
/*!40000 ALTER TABLE `anim_vital` ENABLE KEYS */;
UNLOCK TABLES;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
/*!50003 CREATE*/ /*!50017 DEFINER=`henkhaus`@`%`*/ /*!50003 TRIGGER `zookeeper`.`anim_vital_AFTER_UPDATE` BEFORE UPDATE ON `anim_vital` FOR EACH ROW
BEGIN
	SET new.last_mod_dtm := NOW();
END */;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-12-01 19:27:20
