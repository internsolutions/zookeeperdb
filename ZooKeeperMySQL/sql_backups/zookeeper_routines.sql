-- MySQL dump 10.13  Distrib 5.6.19, for osx10.7 (i386)
--
-- Host: ix.cs.uoregon.edu    Database: zookeeper
-- ------------------------------------------------------
-- Server version	5.6.13

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Dumping events for database 'zookeeper'
--

--
-- Dumping routines for database 'zookeeper'
--
/*!50003 DROP FUNCTION IF EXISTS `GET_DAY_NAME` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` FUNCTION `GET_DAY_NAME`(day_num int) RETURNS varchar(10) CHARSET utf8
BEGIN
	DECLARE day_name VARCHAR(10);
    IF day_num = 1 THEN
		SET day_name = 'Sun';
	ELSEIF day_num = 2 THEN
		SET day_name = 'Mon';
	ELSEIF day_num = 3 THEN
		SET day_name = 'Tue';
	ELSEIF day_num = 4 THEN
		SET day_name = 'Wed';
	ELSEIF day_num = 5 THEN
		SET day_name = 'Thu';
	ELSEIF day_num = 6 THEN
		SET day_name = 'Fri';
	ELSEIF day_num = 7 THEN
		SET day_name = 'Sat';
	ELSE
		SET day_name = 'Unknown';
	END IF;
RETURN (day_name);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP FUNCTION IF EXISTS `GET_TIME_NAME` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` FUNCTION `GET_TIME_NAME`(time_num int) RETURNS varchar(10) CHARSET utf8
BEGIN
	DECLARE time_name VARCHAR(10);
    IF time_num >=0 AND time_num <12 THEN
		SET time_name = 'Morning';
	ELSEIF time_num >=12 AND time_num <16 THEN
		SET time_name = 'Noon';
	ELSEIF time_num >=16 AND time_num <23 THEN
		SET time_name = 'Evening';
	ELSE
		SET time_name = 'Unknown';
	END IF;
    
RETURN (time_name);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_emp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `add_emp`(

	IN 	_first			VARCHAR(100),
    IN 	_last			VARCHAR(100),
    IN	_payroll_add	VARCHAR(150),
    IN	_payroll_city	VARCHAR(45),
	IN	_payroll_st		VARCHAR(45),
    IN	_payroll_zip	VARCHAR(45),
    IN	_ssn			VARCHAR(45),
	IN	_hire_dtm		DATE,
    IN	_emp_ty			CHAR(1),
    IN	_lic_exp_dtm	DATE,
    IN	_wage			FLOAT(15,2),
    OUT	_emp_id			INT(11)

)
BEGIN

-- Insert employee into emp table
	INSERT INTO `zookeeper`.`emp`
		(
		`first`,
		`last`,
		`payroll_add`,
		`payroll_city`,
		`payroll_st`,
		`payroll_zip`,
		`ssn`,
		`hire_dtm`
		)
		VALUES
		(
		_first,
		_last,
		_payroll_add,
		_payroll_city,
		_payroll_st,
		_payroll_zip,
		_ssn,
		_hire_dtm
		);

-- Get and save emp_id in _emp_id
	SET _emp_id = (SELECT LAST_INSERT_ID());
    
-- Check if _emp_ty is either v (for vet) or k (for keeper) then insert into approriate table
	IF _emp_ty = 'v' THEN 
    
	   INSERT INTO `zookeeper`.`vet`
			(
            `emp_id`,
			`lic_exp_dtm`,
			`wage`
            )
			VALUES
			(
            _emp_id,
			_lic_exp_dtm,
			_wage
            );

	ELSEIF _emp_ty = 'k' THEN
    
		INSERT INTO `zookeeper`.`keeper`
			(
            `emp_id`,
			`wage`
            )
			VALUES
			(
            _emp_id,
			_wage
            );

	
	END IF;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_feed_sched` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `add_feed_sched`(

	IN 	_feed_des			VARCHAR(45),
    IN	_su					BIT(1),
    IN	_m					BIT(1),
    IN	_t					BIT(1),
    IN	_w					BIT(1),
    IN	_th					BIT(1),
    IN	_f					BIT(1),
    IN	_sa					BIT(1),
    IN	_morn				BIT(1),
    IN	_noon				BIT(1),
    IN	_even				BIT(1),
    IN	_diet_notes			VARCHAR(200),
    OUT	_sched_id			INT(11)
    

)
BEGIN

-- Insert employee into emp table
	INSERT INTO `zookeeper`.`feed_sched`
		(
		`sched_des`,
		`su_feed_day`,
		`m_feed_day`,
		`tu_feed_day`,
		`w_feed_day`,
		`th_feed_day`,
		`f_feed_day`,
		`sa_feed_day`,
		`m_feed_time`,
		`n_feed_time`,
		`e_feed_time`,
		`diet_notes`)
		VALUES
		(
		_feed_des,
		_su,
		_m,
		_t,
        _w,
        _th,
        _f,
        _sa,
        _morn,
        _noon,
        _even,
		_diet_notes
        );




-- Get and save emp_id in _emp_id
	SET _sched_id = (SELECT LAST_INSERT_ID());
    


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_food_stock` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `add_food_stock`(
	IN 	_food_ty_id		INT(11),
	IN 	_create_dtm		DATETIME,
	IN	_qty_lbs		FLOAT(15,2)
)
BEGIN
	INSERT INTO `zookeeper`.`food_stock`
(
	`food_ty_id`,
	`create_dtm`,
	`qty_lbs`
    )
	VALUES
	(
    _food_ty_id,
	IFNULL(_create_dtm, CURRENT_TIMESTAMP),
	_qty_lbs
    );
	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_food_ty_and_update_stock` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`alishak`@`%` PROCEDURE `add_food_ty_and_update_stock`(
	IN	_food_nm	VARCHAR(45),
    IN 	_manu_id	INT(11),
    IN	_barcode	VARCHAR(45),
    IN 	_qty_lbs	FLOAT(15,2),
    OUT _food_ty_id	INT(11)
)
BEGIN
	
    INSERT INTO `zookeeper`.`food_ty`
	(
	`food_nm`,
	`manu_id`,
	`barcode`
    )
	VALUES
	(
    _food_nm,
	_manu_id,
	_barcode
    );
    
    SET _food_ty_id = (SELECT LAST_INSERT_ID());
    
    INSERT INTO `zookeeper`.`food_stock`
	(
	`food_ty_id`,
	`create_dtm`,
	`qty_lbs`
    )
	VALUES
	(
    _food_ty_id,
	CURRENT_TIMESTAMP,
	_qty_lbs
    );
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_manu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`alishak`@`%` PROCEDURE `add_manu`(
	IN 	_manu_name	VARCHAR(100),
    IN 	_manu_add	VARCHAR(150),
    IN 	_manu_city	VARCHAR(45),
    IN 	_manu_st	VARCHAR(45),
    IN 	_manu_zip	VARCHAR(45),
    IN 	_manu_phone	VARCHAR(45),
    IN 	_manu_cont	VARCHAR(100),
    OUT _manu_id	INT(11)
)
BEGIN

-- Insert manufacturer into food_manu table
	INSERT INTO `zookeeper`.`food_manu`
		(
		`manu_name`,
		`manu_add`,
		`manu_city`,
        `manu_st`,
		`manu_zip`,
		`manu_phone`,
        `manu_cont_nm`
		)
		VALUES
		(
		_manu_name,
        _manu_add,
        _manu_city,
        _manu_st,
        _manu_zip,
        _manu_phone,
        _manu_cont
		);
        
	SET _manu_id = (SELECT LAST_INSERT_ID());

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `add_spec` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`alishak`@`%` PROCEDURE `add_spec`(

	IN _species_name	VARCHAR(100),
    IN _species_desc	VARCHAR(100),
    IN _food_id			INT(11),
    OUT _species_id		INT(11)
    
)
BEGIN

-- Insert species into animal_spec table
	INSERT INTO `zookeeper`.`anim_spec`
		(
		`spec_nm`,
		`spec_des`,
		`food_ty_id`
		)
		VALUES
		(
		_species_name,
		_species_desc,
		_food_id
		);
        
	SET _species_id = (SELECT LAST_INSERT_ID());

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_emp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `delete_emp`(
	IN	_emp_id		INT(11)
)
BEGIN

DELETE FROM vet 
	WHERE emp_id = _emp_id 
	LIMIT 1;
    
DELETE FROM keeper 
	WHERE emp_id = _emp_id 
	LIMIT 1;

DELETE FROM emp 
	WHERE emp_id = _emp_id 
	LIMIT 1;
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `delete_food_stock` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `delete_food_stock`(
	IN 	_food_ty_id		INT(11),
	IN 	_create_dtm		DATETIME,
	IN	_qty_lbs		FLOAT(15,2)
)
BEGIN

DELETE FROM `zookeeper`.`food_stock`
WHERE `food_ty_id` = _food_ty_id AND `create_dtm` = _create_dtm;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_daily_and_past_feeding_due` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_daily_and_past_feeding_due`(

    )
BEGIN
DECLARE BEGINNING_OF_TIME DATE;
DECLARE END_OF_TIME DATE;
SET BEGINNING_OF_TIME = '2015-11-01';
SET END_OF_TIME = DATE(CURRENT_TIMESTAMP);

SELECT *
	FROM 
	(
		SELECT 
			schedule_set.sched_id as schedule_id,
			schedule_set.day_of_week as day_of_week,
			schedule_set.time_of_day as time_of_day,
			anim.anim_id as anim_id,
			schedule_set.selected_date as _date,
            anim.anim_nm as anim_nm
			
		FROM 
			(	SELECT 
					*
				FROM feed_time
				INNER JOIN 

				(select * from 
				(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
				 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
				 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
				 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
				 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
				 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
				 where selected_date between BEGINNING_OF_TIME and END_OF_TIME) dates ON feed_time.day_of_week = DAYOFWEEK(dates.selected_date)
			) as schedule_set

		INNER JOIN anim
			ON schedule_set.sched_id = anim.feed_sched_id
		WHERE schedule_set.selected_date >= anim.aquire_dtm 
		
	) actual_set
    WHERE (schedule_id,day_of_week,time_of_day,anim_id,_date, anim_nm) NOT IN 
		(
        SELECT 
			anim.feed_sched_id as schedule_id,
			DAYOFWEEK(feed_rec.feed_dtm) as day_of_week,
			CASE 
				WHEN HOUR(feed_rec.feed_dtm) between 0 and 12 THEN
					1
				WHEN HOUR(feed_rec.feed_dtm) between 12 and 16 THEN
					2
				WHEN HOUR(feed_rec.feed_dtm) between 16 and 23 THEN
					3
				END as time_of_day,    
			feed_rec.anim_id as anim_id,
			DATE(feed_rec.feed_dtm) as _date,
            anim.anim_nm as anim_nm
		FROM `zookeeper`.`feed_rec`
		INNER JOIN anim
			ON anim.anim_id = feed_rec.anim_id
        )
        ORDER BY _date, day_of_week, time_of_day, anim_id
    ;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_emp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_emp`(
	IN	_emp_id		INT(11)
)
BEGIN

    	SELECT 
		`emp`.`emp_id`,
		`emp`.`first`,
		`emp`.`last`,
		`emp`.`payroll_add`,
		`emp`.`payroll_city`,
		`emp`.`payroll_st`,
		`emp`.`payroll_zip`,
		`emp`.`ssn`,
		`emp`.`hire_dtm`,
        `_emp_typ_table`.emp_ty,
        `_emp_typ_table`.lic_exp_dtm,
        `_emp_typ_table`.wage
	FROM `zookeeper`.`emp`
    LEFT JOIN(	
				(
					SELECT 
						emp_id,
						'v' as emp_ty,
						`vet`.`lic_exp_dtm`,
						`vet`.`wage`
					FROM `zookeeper`.`vet`
					
				)
				UNION
				(
					SELECT 
						emp_id,
						'k' as emp_ty,
						NULL,
						`keeper`.`wage`
					FROM `zookeeper`.`keeper`
					
				)
			) as _emp_typ_table ON emp.emp_id = _emp_typ_table.emp_id
	WHERE emp.emp_id = _emp_id;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_emp_all` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_emp_all`(

)
BEGIN
	SELECT 
		`emp`.`emp_id`,
		`emp`.`first`,
		`emp`.`last`,
		`emp`.`payroll_add`,
		`emp`.`payroll_city`,
		`emp`.`payroll_st`,
		`emp`.`payroll_zip`,
		`emp`.`ssn`,
		`emp`.`hire_dtm`,
        `_emp_typ_table`.emp_ty,
        `_emp_typ_table`.lic_exp_dtm,
        `_emp_typ_table`.wage
	FROM `zookeeper`.`emp`
    LEFT JOIN(	
				(
					SELECT 
						emp_id,
						'v' as emp_ty,
						`vet`.`lic_exp_dtm`,
						`vet`.`wage`
					FROM `zookeeper`.`vet`
					
				)
				UNION
				(
					SELECT 
						emp_id,
						'k' as emp_ty,
						NULL,
						`keeper`.`wage`
					FROM `zookeeper`.`keeper`
					
				)
			) as _emp_typ_table ON emp.emp_id = _emp_typ_table.emp_id
	;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_feed_rec_filter` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_feed_rec_filter`()
BEGIN

	SELECT `feed_rec`.`anim_id`,
		`feed_rec`.`emp_id`,
		`feed_rec`.`feed_amt_lbs`,
		`feed_rec`.`feed_dtm`,
        DAYOFWEEK(`feed_rec`.`feed_dtm`),
        
        CASE 
			WHEN (HOUR(feed_rec.feed_dtm) >= 0) AND (HOUR(feed_rec.feed_dtm) < 12) THEN
				1
			ELSE 0
		END as 'm_feed_time',
        
		CASE
			WHEN HOUR(feed_rec.feed_dtm) >= 12 AND HOUR(feed_rec.feed_dtm) < 16 THEN
				1
			ELSE 0
        END as 'n_feed_time',
        
        CASE
			WHEN HOUR(feed_rec.feed_dtm) >= 16 AND HOUR(feed_rec.feed_dtm) < 23 THEN
				1
			ELSE 0
        END as 'e_feed_time',
        
		`feed_rec`.`last_mod_dtm`
        
        
        
        
        
        
	FROM `zookeeper`.`feed_rec`;





END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_feed_sched` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_feed_sched`(
	IN	_sched_id		INT(11)
)
BEGIN
SELECT 
	`feed_sched`.`sched_id`,
    `feed_sched`.`sched_des`,
    `feed_sched`.`su_feed_day`,
    `feed_sched`.`m_feed_day`,
    `feed_sched`.`tu_feed_day`,
    `feed_sched`.`w_feed_day`,
    `feed_sched`.`th_feed_day`,
    `feed_sched`.`f_feed_day`,
    `feed_sched`.`sa_feed_day`,
    `feed_sched`.`m_feed_time`,
    `feed_sched`.`n_feed_time`,
    `feed_sched`.`e_feed_time`,
    `feed_sched`.`diet_notes`,
    `feed_sched`.`last_mod_dtm`
FROM `zookeeper`.`feed_sched`
WHERE `feed_sched`.`sched_id` = _sched_id;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_food_stock` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_food_stock`(
	IN 	_food_ty_id		INT(11),
	IN 	_create_dtm		DATETIME
)
BEGIN

	SELECT 
		`food_stock`.`food_ty_id`,
        `food_stock`.`qty_lbs`,
		`food_stock`.`create_dtm`
	FROM `zookeeper`.`food_stock`
    WHERE `food_ty_id` = _food_ty_id AND `create_dtm` = _create_dtm;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_food_stock_all` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_food_stock_all`(
	IN 	_LIMIT			INT(11),
	IN 	_OFFSET			INT(11)
)
BEGIN


	IF (    (IFNULL(_LIMIT, 0) != 0)      AND      (IFNULL(_OFFSET, 0) != 0)     ) THEN -- applies both offset and limit to query
		SELECT 
			`food_stock`.`food_ty_id`,
			`food_stock`.`qty_lbs`,
			`food_stock`.`create_dtm`
		FROM `zookeeper`.`food_stock`
		LIMIT _OFFSET, _LIMIT;
        
        
	ELSEIF (IFNULL(_LIMIT, 0) != 0) THEN -- only applying limit with no offset
		SELECT 
			`food_stock`.`food_ty_id`,
			`food_stock`.`qty_lbs`,
			`food_stock`.`create_dtm`
		FROM `zookeeper`.`food_stock`
		LIMIT _LIMIT;
		   
           
    ELSE -- no limit applied, returns all stock records
		SELECT 
			`food_stock`.`food_ty_id`,
			`food_stock`.`qty_lbs`,
			`food_stock`.`create_dtm`
		FROM `zookeeper`.`food_stock`;
        
	END IF;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_food_stock_calc` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_food_stock_calc`(
	IN	_food_ty_id		INT(11)
)
BEGIN


SELECT 
	food_ty.food_ty_id,
    food_ty.food_nm,
    (IFNULL(SUM(food_stock.qty_lbs),0) - IFNULL(SUM(feed_rec.feed_amt_lbs),0)) as 'Current Inventory'
    FROM feed_rec
    INNER JOIN anim 
		ON feed_rec.anim_id = anim.anim_id
	INNER JOIN anim_spec
		ON anim_spec.anim_spec_id = anim.spec_id
	RIGHT JOIN food_ty
		ON anim_spec.food_ty_id = food_ty.food_ty_id
	INNER JOIN food_stock
		ON food_stock.food_ty_id = food_ty.food_ty_id
        
	GROUP BY food_stock.food_ty_id
    HAVING food_ty.food_ty_id = _food_ty_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_food_stock_calc_all` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_food_stock_calc_all`()
BEGIN


SELECT 
	food_ty.food_ty_id,
    food_ty.food_nm,
    (IFNULL(SUM(food_stock.qty_lbs),0) - IFNULL(SUM(feed_rec.feed_amt_lbs),0)) as 'Current Inventory',
    food_manu.manu_id,
    food_manu.manu_name,
    food_ty.barcode
FROM feed_rec
INNER JOIN anim 
	ON feed_rec.anim_id = anim.anim_id
INNER JOIN anim_spec
	ON anim_spec.anim_spec_id = anim.spec_id
RIGHT JOIN food_ty
	ON anim_spec.food_ty_id = food_ty.food_ty_id
INNER JOIN food_stock
	ON food_stock.food_ty_id = food_ty.food_ty_id
INNER JOIN food_manu
	ON food_ty.manu_id = food_manu.manu_id
	
GROUP BY food_stock.food_ty_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_manu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`alishak`@`%` PROCEDURE `list_manu`(
	IN _manu_id	INT(11)
)
BEGIN

	SELECT
		`food_manu`.`manu_id`,
		`food_manu`.`manu_name`,
		`food_manu`.`manu_add`,
		`food_manu`.`manu_city`,
		`food_manu`.`manu_st`,
		`food_manu`.`manu_zip`,
		`food_manu`.`manu_phone`,
		`food_manu`.`manu_cont_nm`
	FROM `zookeeper`.`food_manu`
    WHERE food_manu.manu_id = _manu_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_manu_all` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`alishak`@`%` PROCEDURE `list_manu_all`()
BEGIN

	SELECT
		`food_manu`.`manu_id`,
		`food_manu`.`manu_name`,
		`food_manu`.`manu_add`,
		`food_manu`.`manu_city`,
		`food_manu`.`manu_st`,
		`food_manu`.`manu_zip`,
		`food_manu`.`manu_phone`,
		`food_manu`.`manu_cont_nm`
	FROM `zookeeper`.`food_manu`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_of_dates` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_of_dates`(
	IN	_start	DATE,
    IN	_end	DATE
)
BEGIN

select * from 
(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
where selected_date between _start and _end;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_past_due_feeding` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `list_past_due_feeding`(

    )
BEGIN


DECLARE BEGINNING_OF_TIME DATE;
DECLARE END_OF_TIME DATE;
DECLARE CURR_TIME_OF_DAY INT;
SET BEGINNING_OF_TIME = '2015-11-19';
SET END_OF_TIME = '2020-01-01';

	IF 		(HOUR(CURRENT_TIMESTAMP) >= 0 AND HOUR(CURRENT_TIMESTAMP) < 12) THEN
				SET CURR_TIME_OF_DAY = 1;
	ELSEIF 	(HOUR(CURRENT_TIMESTAMP) >= 12 AND HOUR(CURRENT_TIMESTAMP) < 16) THEN
				SET CURR_TIME_OF_DAY = 2;
	ELSEIF 	(HOUR(CURRENT_TIMESTAMP) >= 16 and HOUR(CURRENT_TIMESTAMP) < 24) THEN
				SET CURR_TIME_OF_DAY = 3;
	ELSE
		SET CURR_TIME_OF_DAY = -1;
	END IF;


SELECT *
	FROM 
	(
		SELECT 
			schedule_set.sched_id as schedule_id,
			schedule_set.day_of_week as day_of_week,
			schedule_set.time_of_day as time_of_day,
			anim.anim_id as anim_id,
			schedule_set.selected_date as _date,
            anim.anim_nm as anim_nm
			
		FROM 
			(	SELECT 
					*
				FROM feed_time
				INNER JOIN 

				(select * from 
				(select adddate('1970-01-01',t4.i*10000 + t3.i*1000 + t2.i*100 + t1.i*10 + t0.i) selected_date from
				 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t0,
				 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t1,
				 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t2,
				 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t3,
				 (select 0 i union select 1 union select 2 union select 3 union select 4 union select 5 union select 6 union select 7 union select 8 union select 9) t4) v
				 where selected_date between BEGINNING_OF_TIME and END_OF_TIME) dates ON feed_time.day_of_week = DAYOFWEEK(dates.selected_date)
			) as schedule_set

		INNER JOIN anim
			ON schedule_set.sched_id = anim.feed_sched_id
	) actual_set
    WHERE (schedule_id,day_of_week,time_of_day,anim_id,_date, anim_nm) NOT IN 
		(
        SELECT 
			anim.feed_sched_id as schedule_id,
			DAYOFWEEK(feed_rec.feed_dtm) as day_of_week,
			CASE 
				WHEN HOUR(feed_rec.feed_dtm) between 0 and 12 THEN
					1
				WHEN HOUR(feed_rec.feed_dtm) between 12 and 16 THEN
					2
				WHEN HOUR(feed_rec.feed_dtm) between 16 and 24 THEN
					3
				END as time_of_day,    
			feed_rec.anim_id as anim_id,
			DATE(feed_rec.feed_dtm) as _date,
            anim.anim_nm as anim_nm
		FROM `zookeeper`.`feed_rec`
		INNER JOIN anim
			ON anim.anim_id = feed_rec.anim_id
        )
        HAVING DATE(time_of_day) <= CURR_TIME_OF_DAY AND _date <= DATE(CURRENT_TIMESTAMP)
        ORDER BY _date, day_of_week, time_of_day, anim_id
    ;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_spec` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`alishak`@`%` PROCEDURE `list_spec`(
	IN _spec_id	INT(11)
)
BEGIN

	SELECT 
		`anim_spec`.`anim_spec_id`,
		`anim_spec`.`spec_nm`,
		`anim_spec`.`spec_des`,
		`anim_spec`.`food_ty_id`
	FROM `zookeeper`.`anim_spec`
    WHERE  anim_spec.anim_species_id = _spec_id; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `list_spec_all` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`alishak`@`%` PROCEDURE `list_spec_all`()
BEGIN

	SELECT 
		`anim_spec`.`anim_spec_id`,
		`anim_spec`.`spec_nm`,
		`anim_spec`.`spec_des`,
		`anim_spec`.`food_ty_id`
	FROM `zookeeper`.`anim_spec`;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_emp` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `update_emp`(

	IN	_emp_id			INT(11),
    IN 	_first			VARCHAR(100),
    IN 	_last			VARCHAR(100),
    IN	_payroll_add	VARCHAR(150),
    IN	_payroll_city	VARCHAR(45),
	IN	_payroll_st		VARCHAR(45),
    IN	_payroll_zip	VARCHAR(45),
    IN	_ssn			VARCHAR(45),
	IN	_hire_dtm		DATE,
    IN	_emp_ty			CHAR(1),
    IN	_lic_exp_dtm	DATE,
    IN	_wage			FLOAT(15,2)

)
BEGIN

-- update employee into emp table
	UPDATE `zookeeper`.`emp`
		SET
			`first` = IFNULL(_first,first),
			`last` = IFNULL(_last,last),
			`payroll_add` = IFNULL(_payroll_add, payroll_add),
			`payroll_city` = IFNULL(_payroll_city, payroll_city),
			`payroll_st` = IFNULL(_payroll_st, payroll_st),
			`payroll_zip` = IFNULL(_payroll_zip, payroll_zip),
			`ssn` = IFNULL(_ssn, ssn),
			`hire_dtm` = IFNULL(_hire_dtm, hire_dtm)
		WHERE `emp_id` = _emp_id;


-- Check if _emp_id is either v (for vet) or k (for keeper) then update approriate table
	IF _emp_ty = 'v' THEN 
    
	   INSERT INTO `zookeeper`.`vet`
			(
            `emp_id`,
			`lic_exp_dtm`,
			`wage`
            )
			VALUES
			(
            _emp_id,
			_lic_exp_dtm,
			_wage
            )
            ON DUPLICATE KEY UPDATE 
            lic_exp_dtm = IFNULL(_lic_exp_dtm, lic_exp_dtm), wage = IFNULL(_wage, wage);

	ELSEIF _emp_id IN (SELECT emp_id FROM vet) AND _emp_ty != 'v' THEN
		DELETE FROM `zookeeper`.`vet`
			WHERE emp_id = _emp_id;
            
    END IF;
    
	IF _emp_ty = 'k' THEN 
    
	   INSERT INTO `zookeeper`.`keeper`
			(
            `emp_id`,
			`wage`
            )
			VALUES
			(
            _emp_id,
			_wage
            )
            ON DUPLICATE KEY UPDATE 
            wage = IFNULL(_wage, wage);

	ELSEIF _emp_id IN (SELECT emp_id FROM keeper) AND _emp_ty != 'k' THEN
		DELETE FROM `zookeeper`.`keeper`
			WHERE emp_id = _emp_id;
            
    END IF;
    
    
    
    
    
    
		

	
	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_feed_sched` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `update_feed_sched`(
	IN	_sched_id			INT(11),
	IN 	_feed_des			VARCHAR(45),
    IN	_su					BIT(1),
    IN	_m					BIT(1),
    IN	_t					BIT(1),
    IN	_w					BIT(1),
    IN	_th					BIT(1),
    IN	_f					BIT(1),
    IN	_sa					BIT(1),
    IN	_morn				BIT(1),
    IN	_noon				BIT(1),
    IN	_even				BIT(1),
    IN	_diet_notes			VARCHAR(200)
    
    

)
BEGIN

-- Insert employee into emp table
UPDATE `zookeeper`.`feed_sched`
SET
`sched_des` = IFNULL(_feed_des,sched_des),
`su_feed_day` = IFNULL(_su,su_feed_day),
`m_feed_day` = IFNULL(_m,m_feed_day),
`tu_feed_day` = IFNULL(_t,tu_feed_day),
`w_feed_day` = IFNULL(_w,w_feed_day),
`th_feed_day` = IFNULL(_th,th_feed_day),
`f_feed_day` = IFNULL(_f,f_feed_day),
`sa_feed_day` = IFNULL(_sa,sa_feed_day),
`m_feed_time` = IFNULL(_morn,m_feed_time),
`n_feed_time` = IFNULL(_noon,n_feed_time),
`e_feed_time` = IFNULL(_even,e_feed_time),
`diet_notes` = IFNULL(_diet_notes,diet_notes)
WHERE `sched_id` = _sched_id;



-- Get and save emp_id in _emp_id
	SET _sched_id = (SELECT LAST_INSERT_ID());
    


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_food_stock` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`henkhaus`@`%` PROCEDURE `update_food_stock`(
	IN 	_food_ty_id		INT(11),
	IN 	_create_dtm		DATETIME,
	IN	_qty_lbs		FLOAT(15,2)
)
BEGIN

	UPDATE `zookeeper`.`food_stock`
		SET
		`food_ty_id` = _food_ty_id,
		`create_dtm` = _create_dtm,
		`qty_lbs` = _qty_lbs
	WHERE `food_ty_id` = _food_ty_id AND `create_dtm` = _create_dtm;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_manu` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`alishak`@`%` PROCEDURE `update_manu`(
	IN  _manu_id	INT(11),
    IN 	_manu_name	VARCHAR(100),
    IN 	_manu_add	VARCHAR(150),
    IN 	_manu_city	VARCHAR(45),
    IN 	_manu_st	VARCHAR(45),
    IN 	_manu_zip	VARCHAR(45),
    IN 	_manu_phone	VARCHAR(45),
    IN 	_manu_cont	VARCHAR(100)

)
BEGIN

-- update specis into anim_spec table
	UPDATE `zookeeper`.`food_manu`
		SET
			`manu_name` = IFNULL(_manu_name,manu_name),
			`manu_add` = IFNULL(_manu_add,manu_add),
			`manu_city` = IFNULL(_manu_city, manu_city),
			`manu_st` = IFNULL(_manu_st,manu_st),
			`manu_zip` = IFNULL(_manu_zip,manu_zip),
			`manu_phone` = IFNULL(_manu_phone, manu_phone),
            `manu_cont_nm` = IFNULL(_manu_cont, manu_cont_nm)
		WHERE `manu_id` = _manu_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `update_spec` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`alishak`@`%` PROCEDURE `update_spec`(

	IN _species_id		INT(11),
    IN _species_name	VARCHAR(100),
    IN _species_desc	VARCHAR(100),
    IN _species_food	INT(11)

)
BEGIN

-- update specis into anim_spec table
	UPDATE `zookeeper`.`anim_spec`
		SET
			`spec_nm` = IFNULL(_species_name,spec_nm),
			`spec_des` = IFNULL(_species_desc,spec_des),
			`food_ty_id` = IFNULL(_species_food, food_ty_id)
		WHERE `anim_spec_id` = _species_id;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2015-11-21 16:38:40
