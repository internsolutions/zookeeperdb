from flask import Flask, render_template, request, redirect, jsonify
from flask.ext.stormpath import StormpathManager, login_required, user, groups_required
import jinja2
from app.dbFunctions import *
from app.validation import *
from app import config
from app.db import *
import datetime
import time
from app import db
import os
# Create Flask App
app = Flask(__name__)


def timeFilter(inTimeOfDay, inTime):
	inTimeOfDay = int(inTimeOfDay)
	if inTimeOfDay == 1:
		if datetime.datetime.now().hour >= 12 :
			tempTime = datetime.datetime.now()
			outTime = tempTime.replace(hour=10)
		else:
			outTime = None

	elif inTimeOfDay == 2:
		if datetime.datetime.now().hour >= 16 :
			tempTime = datetime.datetime.now()
			outTime = tempTime.replace(hour=13)

		elif datetime.datetime.now().hour < 12 :
			tempTime = datetime.datetime.now()
			outTime = tempTime.replace(hour=13)
		else:
			outTime = None

	elif inTimeOfDay == 3:
		if datetime.datetime.now().hour < 16 :
			tempTime = datetime.datetime.now()
			outTime = tempTime.replace(hour=18)
		else:
			outTime = None

	else:
		outTime = None

	return outTime

def getDayandTime(schedule_id=None, arrayIndex=None):
	dayOfWeek = (arrayIndex // 3) + 1
	timeOfDay = arrayIndex % 3
	if timeOfDay == 0:
		timeOfDay = 3
		dayOfWeek = dayOfWeek - 1
	return (schedule_id, dayOfWeek, timeOfDay)

env = jinja2.Environment(extensions=["jinja2.ext.do",])

@app.route('/')
def home():
	return redirect('/schedule')

@app.route('/schedule')
def schedule():
	list, food_list, keepers_list, animals_list = list_todays_schedule()
	current_time = time.strftime("%H")


	return render_template("schedule.html", list=list, food_list=food_list, keepers_list=keepers_list, animals_list=animals_list,  time=int(current_time))

@app.route('/add_feed_rec', methods=['POST'])
def add_feed_rec():
	if request.method == 'POST':
		time_of_day = request.form.get('time_of_day')
		date_time       = None
		emp_id      = request.form.get('emp_id')
		anim_id       = request.form.get('anim_id')
		type      = request.form.get('type')
		amount       = request.form.get('amount')

		date_time = timeFilter(time_of_day, date_time)

		result = add_feed_rec_db(date_time, emp_id, anim_id, amount, type)
	return redirect('/schedule')


@app.route('/animals')
def animals():
	list, food_list = list_species()

	return render_template("animals.html", list=list, food_list=food_list)

@app.route('/add_species', methods=['POST'])
def add_species():
	if request.method == 'POST':

		name       = request.form.get('name')
		desc       = request.form.get('desc')
		food       = request.form.get('food')


		result = add_spec(name, desc, food)

	return redirect('/animals')

@app.route('/species/<id>')
def species(id):
	header, animals, keepers, vets, feed_sched= list_spec_detail(id)

	return render_template("species_detail.html", header=header[0], animals=animals, keepers=keepers, vets=vets, id=id, feed_sched=feed_sched)


@app.route('/add_animal', methods=['POST'])
def add_animal():
	if request.method == 'POST':
		spec_id  = request.form.get('spec_id')
		name       = request.form.get('name')
		sex       = request.form.get('sex')
		dob       = request.form.get('dob')
		sched_id       = request.form.get('sched_id')
		acquired       = request.form.get('acquired')

		result = add_anim(name, spec_id, sched_id, acquired, dob, sex)

	return species(spec_id)

@app.route('/animal/<id>')
def animal(id):
	list, keepers, vets, foods, vet_records, feed_records, sched_list = list_anim(id)
	print vet_records

	return render_template("animal_detail.html", list=list[0], keepers=keepers, foods=foods, vets=vets, vet_records=vet_records, feed_records=feed_records, sched_list=sched_list)

@app.route('/add_feed_rec_detail', methods=['POST'])
def add_feed_rec_detail():
	if request.method == 'POST':
		anim_id = request.form.get('anim_id')
		date_time       = None
		emp_id      = request.form.get('emp_id')

		type      = request.form.get('type')
		amount       = request.form.get('amount')

		result = add_feed_rec_db(date_time, emp_id, anim_id, amount, type)
	return animal(anim_id)

@app.route('/add_vet_rec', methods=['POST'])
def add_vet_rec():
	if request.method == 'POST':
		anim_id = request.form.get('anim_id')
		date_time       = request.form.get('date_time')
		if date_time is None or date_time == '':
			date_time = datetime.datetime.now()
		hr      = request.form.get('hr')

		res      = request.form.get('res')
		temp       = request.form.get('temp')
		weight       = request.form.get('weight')
		note       = request.form.get('note')
		emp_id       = request.form.get('emp_id')

		result = add_vet_rec_db(date_time, anim_id, emp_id, hr, res, temp, weight, note)
	return animal(anim_id)

@app.route('/edit_feed_sched', methods=['POST'])
def edit_feed_sched():
	if request.method == 'POST':
		anim_id = request.form.get('anim_id')
		sched_id       = request.form.get('sched_id')


		result = update_anim(anim_id=anim_id, sched_id=sched_id)
	return animal(anim_id)

@app.route('/history')
def history():
	list, food_list = list_feed_rec_all()
	return render_template("history.html", list=list, food_list=food_list)

@app.route('/update_feed_rec', methods=['POST'])
def update_feed_rec():
	if request.method == 'POST':

		dateTime       = request.form.get('dateTime')
		emp_id      = request.form.get('emp_id')
		animal_id       = request.form.get('animal_id')
		type      = request.form.get('type')
		amount       = request.form.get('amount')

		if amount != "":
			amount = float(amount)


		result = update_feed_rec_db(dateTime, int(emp_id), int(animal_id), int(type), amount)
	return redirect('/history')

@app.route('/delete_feed_rec', methods=['POST'])
def delete_feed_rec():
	if request.method == 'POST':

		dateTime       = request.form.get('dateTime')
		emp_id      = request.form.get('emp_id')
		animal_id       = request.form.get('animal_id')

		result = delete_feed_rec_db(int(animal_id), int(emp_id), dateTime)
	return redirect('/history')



@app.route('/inventory')
def inventory():
	list, records = list_food_stock_calc_all()
	manu_list = list_manu_all()

	return render_template("inventory.html", list=list, manu_list=manu_list, records=records)

@app.route('/delete_inventory_rec', methods=['POST'])
def delete_inventory_rec():
	if request.method == 'POST':

		date_time       = request.form.get('date_time')
		food_id      = request.form.get('food_id')

		result = delete_inventory_rec_db(food_id, date_time)
	return redirect('/inventory')


@app.route('/update_food_stock', methods=['POST'])
def update_food_stock():
	if request.method == 'POST':

		food_id       = request.form.get('food_id')
		amount       = request.form.get('amount')
		if amount != "":
			amount = float(amount)

		result = update_food_stock_db(food_id, None, amount)

	return redirect('/inventory')

@app.route('/submit_manu', methods=['POST'])
def submit_manu():
	if request.method == 'POST':

		name       = request.form.get('name')
		address      = request.form.get('address')
		city       = request.form.get('city')
		state    = request.form.get('state')
		zipcode        = request.form.get('zipcode')
		phone    = request.form.get('phone')
		contact         = request.form.get('contact')

		result = add_manu(name, address, city, state, zipcode, phone, contact)

	return redirect('/inventory')

@app.route('/edit_manu', methods=['POST'])
def edit_manu():
	if request.method == 'POST':
		id          = request.form.get('id')
		name       = request.form.get('name')
		address      = request.form.get('address')
		city       = request.form.get('city')
		state    = request.form.get('state')
		zipcode        = request.form.get('zipcode')
		phone    = request.form.get('phone')
		contact         = request.form.get('contact')

		result = update_manu(id, name, address, city, state, zipcode, phone, contact)

	return redirect('/inventory')

@app.route('/add_food_type', methods=['POST'])
def add_food_type():
	if request.method == 'POST':
		name       = request.form.get('name')
		manu_id      = request.form.get('manu_id')
		barcode       = request.form.get('barcode')
		quantity    = request.form.get('quantity')

		if quantity != "":
			quantity = float(quantity)


		result = add_food_ty_and_update_stock(name, manu_id, barcode, quantity)

	return redirect('/inventory')

@app.route('/employees')
def employees():

	list, spec_list = list_emp_and_spec_all()
	empDict = {}
	for employee in list:
		employeeHashString = str(employee[0]) + '/' + str(employee[12])
		empDict[employeeHashString] = 0


	return render_template("employees.html", list=list, spec_list=spec_list, emp_spec_dict=empDict)



@app.route('/add_employee', methods=['POST'])
def add_employee():

	if request.method == 'POST':

		first       = request.form.get('fname')
		last        = request.form.get('lname')
		address      = request.form.get('address')
		city       = request.form.get('city')
		state    = request.form.get('state')
		zipcode        = request.form.get('zipcode')
		ssn    = request.form.get('ssn')
		position         = request.form.get('position')
		license  = request.form.get('license')
		wage       = request.form.get('wage')
		hired       = request.form.get('hired')
		spec_id     = request.form.getlist('animal_list')


		if position == "vet":
			position = 'v'
		else:
			position = 'k'

		if wage != "":
			wage = float(wage)

		if hired == "":
			hired = None
		else:
			hired = datetime.datetime.strptime(hired, '%Y-%m-%d').date()

		if license == "":
			license = None
		else:
			license = datetime.datetime.strptime(license, '%Y-%m-%d').date()


		emp_id = add_emp(first, last, address, city, state, zipcode, ssn, hired, position, license, wage)
		for id in spec_id:
			add_emp_spec(id, emp_id, position)






	return redirect('/employees')


@app.route('/update_employee', methods=['POST'])
def update_employee():

	if request.method == 'POST':
		emp_id = request.form.get('id')
		first       = request.form.get('fname')
		last        = request.form.get('lname')
		address      = request.form.get('address')
		city       = request.form.get('city')
		state    = request.form.get('state')
		zipcode        = request.form.get('zipcode')
		ssn    = request.form.get('ssn')
		position         = request.form.get('position')
		license  = request.form.get('license')
		wage       = request.form.get('wage')
		hired       = request.form.get('hired')
		spec_id     = request.form.getlist('animal_list')

		if position == "vet":
			position = 'v'
		else:
			position = 'k'

		if license == "None":
			license = '0000-00-00'

		if wage != "":
			wage = float(wage)


		result = update_emp(emp_id, first, last, address, city, state, zipcode, ssn, hired, position, license, wage)
		delete_emp_spec_all(emp_id, position)
		for id in spec_id:
			add_emp_spec(id, emp_id, position)


	return redirect('/employees')

@app.route('/feeding-schedule')
def feeding_schedule():

	list, schedule_times = list_feeding_schedule_all()

	weekdays = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"]
	schedules = {}


	# print "schedule times:", schedule_times
	weekTable = [[0]*3 for _ in range(7)]

	previous = None
	for schedule in schedule_times:
		if schedule[0] != previous:
			if previous is not None:
				schedules[previous] = weekTable
			# print weekTable
			weekTable = [[0]*3 for _ in range(7)]


		day = schedule[1]-1
		time = schedule[2]-1

		weekTable[day][time] = 1

		previous = schedule[0]
	if previous is not None:
		schedules[previous] = weekTable

	return render_template("feeding_schedule.html", list=list, schedule_times=schedule_times, weekTableFormat = schedules, weekdays=weekdays)

@app.route('/edit_feeding_sched', methods=['POST'])
def edit_feeding_sched():

	if request.method == 'POST':
		sched_id       = request.form.get('sched_id')
		desc       = request.form.get('desc')
		note        = request.form.get('notes')
		sched     = request.form.getlist('sched')

		update_feed_sched(sched_id, desc, note)

		delete_sched_time_all_by_id(sched_id)
		schedForMany = []
		for checkBox in sched:
			# add_sched_time(getDayandTime(sched_id, int(checkBox)))
			schedForMany.append(getDayandTime(sched_id, int(checkBox)))
		db.bulkInsertSP('call add_sched_time(%s, %s, %s)', schedForMany)
	return redirect('/feeding-schedule')



@app.route('/add_feeding_sched', methods=['POST'])
def add_feeding_sched():

	if request.method == 'POST':
		desc       = request.form.get('desc')
		note        = request.form.get('notes')
		sched     = request.form.getlist('sched')

		sched_id = add_feed_sched(desc, note)
		schedForMany = []
		for checkBox in sched:
			# add_sched_time(getDayandTime(sched_id, int(checkBox)))
			schedForMany.append(getDayandTime(sched_id, int(checkBox)))
		db.bulkInsertSP('call add_sched_time(%s, %s, %s)', schedForMany)


	return redirect('/feeding-schedule')

if __name__ == '__main__':
	ip=None
	if os.getenv('ZooKeeperIP') :
		print 'Found IP Address env var.'
		ip = os.environ['ZooKeeperIP']

	app.run(debug=True, host=ip, port=5000)
