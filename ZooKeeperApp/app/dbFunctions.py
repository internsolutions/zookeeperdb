
from app.db import *

#######################
#    Employee Page    #
#######################
def list_emp_and_spec_all():
	# Lists all employees
	try:
		results = storedProcedureList(procedure='list_emp_and_spec_all', parameter=())
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results

def add_emp(firstname="",	lastname="", address="", city="", state="", zipcode="", ssn="", hire=0, type=0, license=0, wage=0):
	try:
		results = storedProcedureArgs(procedure='add_emp', parameter=(firstname, lastname, address, city, state, zipcode, ssn, hire, type, license, wage, 0))[11]

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results


def update_emp(id=0, firstname="",	lastname="", address="", city="", state="", zipcode="", ssn="", hire=0, type=0, license=0, wage=0):
	try:
		results = storedProcedureArgs(procedure='update_emp', parameter=(id, firstname, lastname, address, city, state, zipcode, ssn, hire, type, license, wage))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def delete_emp_spec_all(id=0, type=''):
	try:
		results = storedProcedureArgs(procedure='delete_emp_spec_all', parameter=(id, type))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def add_emp_spec(spec_id=0, emp_id=0, type=''):
	try:
		results = storedProcedureArgs(procedure='add_emp_spec', parameter=(spec_id, emp_id, type))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results
########################
#    Inventory Page    #
########################

def list_food_stock_calc_all(param=0):
	# Lists all employees
	try:
		results = storedProcedureList(procedure='list_food_stock_calc_all', parameter=())
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results

def list_daily_and_past_feeding(date=0):
	try:
		results = storedProcedureList(procedure='list_daily_and_past_feeding_due', parameter=(date))[0]
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results

def list_manu_all():
	try:
		results = storedProcedureList(procedure='list_manu_all', parameter=())[0]
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results

def add_manu(name="", address="", city="", state="", zipcode="", phone="", contact=""):
	try:
		results = storedProcedureArgs(procedure='add_manu', parameter=(name, address, city, state, zipcode, phone, contact, 0))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def update_manu(id=0, name="", address="", city="", state="", zipcode="", phone="", contact=""):
	try:
		results = storedProcedureArgs(procedure='update_manu', parameter=(id, name, address, city, state, zipcode, phone, contact))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def add_food_ty_and_update_stock(name, manu_id, barcode, quantity):
	try:
		results = storedProcedureArgs(procedure='add_food_ty_and_update_stock', parameter=(name, manu_id, barcode, quantity, 0))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def update_food_stock_db(food_id=0, date_time=None, amount=0):
	try:
		results = storedProcedureArgs(procedure='update_food_stock', parameter=(food_id, date_time, amount))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def delete_inventory_rec_db(food_id=0, date_time=""):
	try:
		results = storedProcedureArgs(procedure='delete_food_stock', parameter=(food_id, date_time))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

######################
#    Species Page    #
######################
def list_species():
	try:
		results = storedProcedureList(procedure='list_spec_all', parameter=())
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results


def add_spec(name="", desc="", id=0):
	try:
		results = storedProcedureArgs(procedure='add_spec', parameter=(name, desc, id, 0))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

#############################
#    Species Detail Page    #
#############################

def list_spec_detail(id=0):
	try:
		results = storedProcedureList(procedure='list_spec', parameter=(id,))
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results



############################
#    Animal Detail Page    #
############################

def add_anim(name="", spec_id=0, sched_id=0, acquired="", dob="", sex=""):
	try:
		results = storedProcedureArgs(procedure='add_anim', parameter=(name, spec_id, sched_id, acquired, dob, sex))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def list_anim(anim_id=0):
	try:
		results = storedProcedureList(procedure='list_anim', parameter=(anim_id,))
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results

def add_vet_rec_db(date_time=None , anim_id=0, emp_id=0, hr=0, res=0, temp=0, weight=0, note="" ):
	try:
		results = storedProcedureArgs(procedure='add_anim_vital', parameter=(date_time, anim_id, emp_id, hr, res, temp, weight, note))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def update_anim(anim_id=0, anim_nm=None, spec_id=None, sched_id=None, acquired_dtm=None, dob=None, sex=None ):
	try:
		results = storedProcedureArgs(procedure='update_anim', parameter=(anim_id, anim_nm, spec_id, sched_id, acquired_dtm, dob, sex))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results




######################
#    History Page    #
######################

def list_feed_rec_all():
	try:
		results = storedProcedureList(procedure='list_feed_rec_all', parameter=())
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results

def update_feed_rec_db(date_time="", emp_id=0, animal_id=0, type=0, amount=0):
	try:
		results = storedProcedureArgs(procedure='update_feed_rec', parameter=(date_time, emp_id, animal_id, amount, type))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def delete_feed_rec_db(animal_id=0, emp_id=0, date_time=""):
	try:
		results = storedProcedureArgs(procedure='delete_feed_rec', parameter=(animal_id, emp_id, date_time))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

#######################
#    Today's Schedule Page    #
#######################

def list_todays_schedule():
	try:
		results = storedProcedureList(procedure='list_todays_schedule', parameter=())
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results

def add_feed_rec_db(date_time="" , emp_id=0, animal_id=0, amount=0, type=0 ):
	try:
		results = storedProcedureArgs(procedure='add_feed_rec', parameter=(date_time, emp_id, animal_id, amount, type))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results


#######################
#    Schedule Page    #
#######################

def list_feeding_schedule_all():
	try:
		results = storedProcedureList(procedure='list_feed_sched_all', parameter=())
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results

def delete_sched_time_all_by_id(sched_id=0 ):
	try:
		results = storedProcedureArgs(procedure='delete_sched_time_all_by_id', parameter=(sched_id,))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def add_sched_time(sched_time=()):
	try:
		results = storedProcedureArgs(procedure='add_sched_time', parameter= sched_time)

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def add_feed_sched(desc="", note="" ):
	try:
		results = storedProcedureArgs(procedure='add_feed_sched', parameter=(desc, note, 0))[2]

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

def update_feed_sched(sched_id=0, desc="", note="" ):
	try:
		results = storedProcedureArgs(procedure='update_feed_sched', parameter=(sched_id, desc, note))

	except mysql.connector.Error as err:

		print("Something went wrong: {}".format(err))
		return None
	return results

"""
EXAMPLE STORED PROCEDURE FUNCTIONS

def nameOfStoredProcedure(param=0):
	# Description of Stored Procedure
	try:
		results = storedProcedureList(procedure='nameOfSP', parameter=(param,))
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return results


def nameOfStoredProcedure(param=0):
	# Description of Stored Procedure
	try:
		retrunArgs = storedProcedureArgs(procedure='nameOfSP', parameter=(param,))
	except mysql.connector.Error as err:
		print("Something went wrong: {}".format(err))
		return None
	return retrunArgs


"""