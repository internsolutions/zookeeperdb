__author__ = 'alishakawaguchi'


import re


def validation(firstname="",lastname="", duckid="", idnumber="", phonenumber="",  year="", act=None, returning=None, pos=None, optIn=0):
	errorMsg = {}
	flag = True

	if not re.match("^[a-zA-Z\'-]+$", firstname):
		errorMsg['firstName'] = "Error with First Name"
		flag = False

	if not re.match("^[a-zA-Z\'-]+$", lastname):
		errorMsg['lastName'] = "Error with Last Name"
		flag = False

	if len(year) == 0:
		errorMsg['year'] = "Error with year"
		flag = False

	if not re.match("^[a-zA-Z]+$", duckid):
		errorMsg['duckId'] = "Error with Duck ID"
		flag = False

	if not (re.match("^[0-9]+$", idnumber) and len(idnumber) == 9):
		errorMsg['idNum'] = "Error with Student ID Number"
		flag = False

	if not (re.match("^[0-9]+$", phonenumber) and len(phonenumber) == 10):
		errorMsg['phoneNum'] = "Error with Phone Number"
		flag = False

	if act is None:
		errorMsg['act'] = "Please indicate if you are willing to act"
		flag = False

	if returning is None :
		errorMsg['returnDuck'] = "Please indicate if you are returning to Duck TV"
		flag = False

	if pos is None or len(pos) == 0:
		errorMsg['appPositions'] = "Please select a position"
		flag = False

	if optIn != 1:
		errorMsg['optIn'] = "Please opt in to sharing your information"
		flag = False


	return errorMsg


