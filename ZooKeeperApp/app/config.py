__author__ = 'gregoryhenkhaus'


# Organization Details
organizationName = 'ZooKeeper'

# Database Connection
DBuser              = 'application'
DBpassword          = 'intern'
DBhost              = 'ix.cs.uoregon.edu'
DBport              = '3336'
DBdatabase          = 'zookeeper'
DBraise_on_warnings = True
DBautocommit        = True
DBpool_name         = 'zookeeperpool'
DBpool_size         = 4
